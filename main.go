package main

import (
	"nine/src/config"
	"nine/src/web"
)

func main() {
	config.Load()
	web.Run()
}
