package config

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	WebPort   = 0
	SecretKey []byte

	TokenExpDuration = 12 * time.Hour
	ServerURL        = ""
	IsRunningLocally = true

	GoogleOAuth oauth2.Config

	MailRedirectURL = ""
	Sender = ""
)

func Load() {
	var err error
	//if err = godotenv.Load(); err != nil {
	//	log.Panic("Failed to load environment variables")
	//}

	exp, err := strconv.Atoi(os.Getenv("TOKEN_EXP_DURATION"))
	if err != nil {
		exp = 60
	}
	TokenExpDuration = time.Duration(exp) * time.Minute

	local := strings.ToLower(strings.TrimSpace(os.Getenv("LOCAL_OR_REMOTE")))
	if local == "remote" {
		IsRunningLocally = false
	}

	WebPort, err = strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		WebPort = 5000
	}

	SecretKey = []byte(os.Getenv("API_SECRET"))

	redirect := fmt.Sprintf("http://localhost:%d", WebPort)
	if IsRunningLocally == false {
		redirect = "https://nine-web-server.herokuapp.com"
	}

	// Setup google oauth configs
	GoogleOAuth.RedirectURL = fmt.Sprintf("%s/google/auth", redirect)
	GoogleOAuth.ClientID = os.Getenv("GOOGLE_CLIENT_ID")
	GoogleOAuth.ClientSecret = os.Getenv("GOOGLE_CLIENT_SECRET")
	GoogleOAuth.Scopes = []string{"https://www.googleapis.com/auth/userinfo.email"}
	GoogleOAuth.Endpoint = google.Endpoint

	if IsRunningLocally == true {
		redirect = fmt.Sprintf("http://localhost:%d", WebPort)
	}else {
		redirect = "https://nine-web-server.herokuapp.com"
	}
	MailRedirectURL = fmt.Sprintf("%s/password/reset", redirect)
	Sender = "NI NE Web Server"

	if IsRunningLocally == true {
		redirect = fmt.Sprintf("http://localhost:%d", 9000)
	} else {
		redirect = "https://nine-api-server.herokuapp.com"
	}
	// Setup Server vars
	ServerURL = fmt.Sprintf("%s", redirect)
}
