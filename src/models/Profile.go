package models

import "time"

const (
	FEMALE    = 1
	MALE      = 2
	NOTBINARY = 3
)

type Profile struct {
	ID   uint32 `json:"id"`
	User User   `gorm:"foreignkey:ID" json:"-"`

	Name    *string `gorm:"size:254" json:"name"`
	Phone   *string `gorm:"size:20" json:"phone"`
	Gender  uint8   `gorm:"not null" json:"gender"`
	Address *string `gorm:"size:254" json:"address"`

	CreatedAt time.Time `gorm:"default:current_timestamp()" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:current_timestamp()" json:"updated_at"`
}
