package models

import (
	"time"
)

type User struct {
	ID       uint32 `gorm:"primary_key;auto_increment" json:"id"`
	Email    string `gorm:"size:254;not null;unique" json:"email"`
	Password string `gorm:"size:63;not null" json:"password,omitempty"`

	CreatedAt time.Time `gorm:"default:current_timestamp()" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:current_timestamp()" json:"updated_at"`
}
