// const Uri = "http://localhost:5000"
const Uri = "https://nine-web-server.herokuapp.com"
let auth_token = sessionStorage.auth_token
cache = {}

// Remove all un-necessary console logs
console.log = function () {
}

const NAME_FIELD = "name"
const ADDRESS_FIELD = "address"
const PHONE_FIELD = "phone"
const EMAIL_FIELD = "email"

let httpStatus = {
    OK: 200,
    Created: 201,

    BadRequest: 400,
    Unauthorized: 401,
    NotFound: 404
}


const mainHeader = document.createElement("h1")
mainHeader.textContent = "9"
mainHeader.setAttribute("class", "mainHeader")


function createTextInput(input, type) {
    const inputContainer = document.createElement("div")
    inputContainer.setAttribute("class", input + "Container")

    const inputElement = document.createElement("input")
    inputElement.setAttribute("class", input + "Element")
    inputElement.type = type
    inputElement.id = input + "Input"
    inputElement.name = input + "-input"

    const inputLabel = document.createElement("label")
    inputLabel.setAttribute("class", input + "Label")
    inputLabel.textContent = capFirst(input) + " "
    inputLabel.setAttribute("for", input + "Input")

    const errorLabel = document.createElement("label")
    errorLabel.setAttribute("class", input + "ErrorLabel")
    errorLabel.textContent = ""
    errorLabel.setAttribute("for", input + "Input")

    inputContainer.append(inputLabel)
    inputContainer.append(inputElement)
    inputContainer.append(errorLabel)

    return inputContainer
}

function getTextInput(input) {
    const inputs = document.getElementsByClassName(input + "Element")
    return inputs[0].value
}

function createButton(text, callback) {
    const button = document.createElement("button")
    button.className = text + "Button Button"
    button.textContent = text
    button.onclick = callback

    return button
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}

function validatePassword(password) {
    if (password.length < 5) {
        return false
    } else if (password.length > 50) {
        return false
    }

    return true
}

function validatePhone(phone) {
    const re = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d+\)?[\-\.\ \\\/]?)*)(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/
    return re.test(phone)
}

function createTextField(field) {
    const singleInfoContainer = document.createElement("div")
    singleInfoContainer.className = field + "InfoContainer InfoContainer"

    const fieldContainer = document.createElement("span")
    fieldContainer.className = field + "FieldContainer FieldContainer"

    const valContainer = document.createElement("span")
    valContainer.className = field + "ValueContainer ValueContainer"
    fieldContainer.textContent = capFirst(field) + " "

    singleInfoContainer.append(fieldContainer)
    singleInfoContainer.append(valContainer)

    return singleInfoContainer
}

function createError(error) {

    const errContainer = document.createElement("span")
    errContainer.className = getFirstWord(error) + "ErrorContainer ErrorContainer"

    return errContainer
}

function capFirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

function getFirstWord(str) {
    let spaceIndex = str.indexOf(' ');
    return spaceIndex === -1 ? str : str.substr(0, spaceIndex);
};

// Make a get request to both user and profile and obtain the information
async function cacheUserProfile() {
    let userGet = new XMLHttpRequest()
    userGet.open("GET", Uri + "/users?token=" + auth_token, true)

    let userGetPromise = new Promise(resolve => {
        userGet.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                let data = JSON.parse(this.response)
                cache[EMAIL_FIELD] = data[EMAIL_FIELD]
            }
            resolve()
        }
    })

    let profileGet = new XMLHttpRequest()
    profileGet.open("GET", Uri + "/users/profile?token=" + auth_token, true)

    let profileGetPromise = new Promise(resolve => {
        profileGet.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                let data = JSON.parse(this.response)
                cache[NAME_FIELD] = data[NAME_FIELD]
                cache[PHONE_FIELD] = data[PHONE_FIELD]
                cache[ADDRESS_FIELD] = data[ADDRESS_FIELD]
            }
            resolve()
        }
    })

    userGet.send()
    profileGet.send()

    let promise = new Promise(resolve => {
        Promise.all([userGetPromise, profileGetPromise]).then(() => {
            console.log("User Profile info obtained & cached")
            resolve()
        })
    })

    await promise
}

function clearAuthToken() {
    sessionStorage.google_auth = ""
    sessionStorage.auth_token = ""
}
