document.getElementsByClassName("emailElement")[0].value = ""

function sendResetLink() {
    document.getElementsByClassName("forgotErrorContainer")[0].innerHTML = ""
    document.getElementsByClassName("sending-mail")[0].innerHTML = ""
    document.getElementsByClassName("sending-mail")[0].style.color = "antiquewhite"

    const email = getTextInput("email")

    if (validateEmail(email)) {
        document.getElementsByClassName("sending-mail")[0].innerHTML = "Sending Mail ..."
        console.log("Sending Password Reset Link")

        // Create a request variable and assign XMLHttpRequest object
        let request = new XMLHttpRequest()

        request.open("POST", Uri + "/password/forgot", true)
        request.setRequestHeader("Content-Type", "application/json")

        const cred = {
            email: email
        }
        request.onload = function () {
            let data = JSON.parse(this.response)
            console.log("Obtained Data", data)

            if (this.status < 200 || this.status >= 400) {
                document.getElementsByClassName("sending-mail")[0].style.color = "red"
                if (this.status === httpStatus.NotFound) {
                    document.getElementsByClassName("sending-mail")[0].innerHTML = "Email Not Found!!"
                } else {
                    document.getElementsByClassName("forgotErrorContainer")[0].innerHTML = "*" + data.error

                    document.getElementsByClassName("sending-mail")[0].innerHTML = "Mail Could Not Be Sent!!"
                }
            } else {
                // Everything is fine now
                document.getElementsByClassName("sending-mail")[0].innerHTML = "Mail Sent Successfully!!"
                document.getElementsByClassName("sending-mail")[0].style.color = "green"

                setTimeout(()=> {
                    let k = 5
                    setInterval(()=> {
                        if (k >= 0) {
                            document.getElementsByClassName("sending-mail")[0].innerHTML =
                                "You will be redirected to login page in " + k + "s"
                        }
                        if (k <= 0) {
                            window.location.href = "index.html"
                        }
                        k--
                    }, 1000)
                }, 2000)
            }
        }

        request.onerror = function () {
            console.log("Simply Error")
            document.getElementsByClassName("sending-mail")[0].innerHTML = "Mail Could Not Be Sent!!"
            document.getElementsByClassName("sending-mail")[0].style.color = "red"
        }
        request.send(JSON.stringify(cred))
    } else {
        document.getElementsByClassName("emailErrorLabel")[0].innerHTML = "*Invalid Email"
    }
}
