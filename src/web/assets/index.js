clearAuthToken()

function loginCallback() {
    // Reset Error Messages
    document.getElementsByClassName("passwordErrorLabel")[0].innerHTML = ""
    document.getElementsByClassName("emailErrorLabel")[0].innerHTML = ""
    document.getElementsByClassName("loginErrorContainer")[0].innerHTML = ""

    const email = getTextInput("email")

    if (validateEmail(email)) {
        const password = getTextInput("password")

        // Get Login Token
        if (validatePassword(password)) {
            // Create a request variable and assign XMLHttpRequest object
            let request = new XMLHttpRequest()

            request.open("POST", Uri + "/login", true)
            request.setRequestHeader("Content-Type", "application/json")

            const cred = {
                email: email,
                password: password
            }
            request.onload = function () {
                let data = JSON.parse(this.response)

                if (this.status < 200 || this.status >= 400) {
                    document.getElementsByClassName("loginErrorContainer")[0].innerHTML = "*" + data.error
                } else {
                    // Everything is fine now
                    sessionStorage.auth_token = data.token
                    window.location.href = "/profile_edit.html"
                }
            }
            request.send(JSON.stringify(cred))

        } else {
            document.getElementsByClassName("passwordErrorLabel")[0].innerHTML = "*Invalid Password"
        }
    } else {
        document.getElementsByClassName("emailErrorLabel")[0].innerHTML = "*Invalid Email"
    }
}

function googleLoginCallback() {
    // Create a request variable and assign XMLHttpRequest object
    window.location.href = "/google/login"
}
