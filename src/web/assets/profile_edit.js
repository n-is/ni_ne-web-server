if (auth_token === "") {
    // Redirect to home page
    console.log("Can't edit profile without JWT token")
    window.location.href = "index.html"
}
document.getElementsByClassName("profile-edit")[0].style.visibility = "visible"

let cachePromise = cacheUserProfile()

Promise.all([cachePromise]).then(() => {
    // Update Input contents with the cached data

    let fields = [NAME_FIELD, ADDRESS_FIELD, PHONE_FIELD, EMAIL_FIELD]
    fields.filter(field => (cache[field] !== undefined)).map(field => {
        document.getElementsByClassName(field + "Element")[0].value = cache[field]
    })

    if (sessionStorage.google_auth === "true") {
        document.getElementsByClassName("emailElement")[0].setAttribute("readonly", true)
    }
})

function saveAndContinue() {
    // Reset error labels
    document.getElementsByClassName("emailErrorLabel")[0].innerHTML = ""
    document.getElementsByClassName("phoneErrorLabel")[0].innerHTML = ""

    const name = getTextInput(NAME_FIELD)
    const address = getTextInput(ADDRESS_FIELD)
    const phone = getTextInput(PHONE_FIELD)
    const email = getTextInput(EMAIL_FIELD)

    if (!validateEmail(email)) {
        document.getElementsByClassName("emailErrorLabel")[0].innerHTML = "*Invalid Email"
        return
    }

    if (!validatePhone(phone)) {
        document.getElementsByClassName("phoneErrorLabel")[0].innerHTML = "*Invalid Phone"
        return
    }

    // Send PUT Request to user if there is change in email
    let userPromise = new Promise(resolve => {
        if (email !== cache[EMAIL_FIELD]) {
            let cred = {}
            cred[EMAIL_FIELD] = email

            let userUpdate = new XMLHttpRequest()
            userUpdate.open("PUT", Uri + "/users?token=" + auth_token, true)
            userUpdate.setRequestHeader("Content-Type", "application/json")

            userUpdate.onload = function () {
                if (this.status === 401) {
                    // Unauthorized Person here
                    window.location.href = "/"
                }
                console.log("User updated")
                cache[EMAIL_FIELD] = email
                resolve()
            }
            userUpdate.send(JSON.stringify(cred))
        } else {
            resolve()
        }
    })

    let profilePromise = new Promise(resolve => {
        let profileSet = new XMLHttpRequest()
        const url = Uri + "/users/profile?token=" + auth_token

        // Send PUT request for the profile update
        profileSet.open("PUT", url, true)

        profileSet.onload = function () {
            console.log("Profile updated")
            cache[NAME_FIELD] = name
            cache[PHONE_FIELD] = phone
            cache[ADDRESS_FIELD] = address

            resolve()
        }

        let profile = {}
        profile[NAME_FIELD] = name
        profile[PHONE_FIELD] = phone
        profile[ADDRESS_FIELD] = address

        profileSet.send(JSON.stringify(profile))
    })

    // Load new page if the info have been updated
    Promise.all([userPromise, profilePromise]).then(() => window.location.href = "profile_read.html")
}

function cancelInfo() {
    console.log("Cancelled")
    window.location.href = "profile_read.html"
}
