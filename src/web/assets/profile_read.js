if (auth_token === "") {
    // Redirect to home page
    console.log("Can't read profile without JWT token")
    window.location.href = "index.html"
}
document.getElementsByClassName("profile-read")[0].style.visibility = "visible"

let cachePromise = cacheUserProfile()

Promise.all([cachePromise]).then(() => {
    // Update Input contents with the cached data
    let fields = [NAME_FIELD, ADDRESS_FIELD, PHONE_FIELD, EMAIL_FIELD]
    fields.filter(field => (cache[field] !== undefined)).map(field => {
        document.getElementsByClassName(field + "ValueContainer")[0].innerHTML = cache[field]
    })
})

function edit() {
    console.log("Edit Info")
    window.location.href = "profile_edit.html"
}

function logout() {
    console.log("Trying to log out")
    clearAuthToken()
    window.location.href = "index.html"
}
