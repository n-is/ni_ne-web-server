if (sessionStorage.pass_auth_token === "" || sessionStorage.pass_auth_token === undefined) {
    // Redirect to home page
    console.log("Can't reset password without valid token")
    document.getElementsByClassName("resetPasswordErrorContainer")[0].innerHTML = "Password Reset Token Failed To Obtain"
}
document.getElementsByClassName("resetPasswordErrorContainer")[0].style.color = "red"
document.getElementsByClassName("resetPasswordErrorContainer")[0].style.fontSize = "1em"

function enterNewPassword() {
    console.log(sessionStorage.pass_auth_token)
    if (sessionStorage.pass_auth_token === "" || sessionStorage.pass_auth_token === undefined) {
        document.getElementsByClassName("resetPasswordErrorContainer")[0].innerHTML = "Password Reset Token Failed To Obtain"
    } else {
        document.getElementsByClassName("resetPasswordErrorContainer")[0].innerHTML = ""
        document.getElementsByClassName("newPasswordErrorLabel")[0].innerHTML = ""
        document.getElementsByClassName("confirmPasswordErrorLabel")[0].innerHTML = ""

        const newPassword = getTextInput("newPassword")
        const confirmPassword = getTextInput("confirmPassword")

        if (validatePassword(newPassword)) {
            if (confirmPassword === newPassword) {
                let request = new XMLHttpRequest()

                request.open("POST", Uri + "/password/new?token=" + sessionStorage.pass_auth_token, true)
                request.setRequestHeader("Content-Type", "application/json")

                const cred = {
                    password: newPassword
                }

                request.onload = function () {
                    let data = JSON.parse(this.response)

                    if (this.status < 200 || this.status >= 400) {
                        document.getElementsByClassName("resetPasswordErrorContainer")[0].innerHTML = "*" + data.error
                    } else {
                        // Everything is fine now
                        sessionStorage.auth_token = data.token
                        window.location.href = "/profile_edit.html"
                    }
                }

                request.onerror = function () {
                    document.getElementsByClassName("resetPasswordErrorContainer")[0].innerHTML = "Failed to Reset Password"
                }
                request.send(JSON.stringify(cred))
            } else {
                document.getElementsByClassName("confirmPasswordErrorLabel")[0].innerHTML = "*Password Mismatch"
            }
        } else {
            document.getElementsByClassName("newPasswordErrorLabel")[0].innerHTML = "*Invalid Password"
        }
    }
}
