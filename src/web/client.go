package web

import (
	"fmt"
	"log"
	"net/http"
	"nine/src/config"
	"nine/src/web/controllers"
	"nine/src/web/router"
)

func Run() {
	config.Load()
	controllers.SetupAfterLoginCallback("/profile_edit.html")

	r := router.NewRouter()
	log.Println("Listening ... :", config.WebPort)
	err := http.ListenAndServe(fmt.Sprintf(":%d", config.WebPort), r)
	if err != nil {
		log.Println(err)
	}
}
