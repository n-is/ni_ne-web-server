package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"nine/src/config"
	"nine/src/jwtok"
	"nine/src/models"
	"nine/src/responses"
)

var mailMessage = `
Heard you lost your password? Let us get you into the system now.

Follow the link below to reset your password.
`

func ForgotPassword(w http.ResponseWriter, r *http.Request) {
	fmt.Println("I am here!!")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	email := emailOnly{}
	if err = json.Unmarshal(body, &email); err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	redirectInfo := models.EmailRedirectInfo{
		Email:    email.Email,
		Redirect: config.MailRedirectURL,
		Message:  mailMessage,
		Sender:   config.Sender,
	}
	redirect, _ := json.Marshal(redirectInfo)

	forgotPage := fmt.Sprintf("%s/password/forgot", config.ServerURL)

	response, err := http.Post(forgotPage, "application/json", bytes.NewBuffer(redirect))
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK {
		responses.JSON(w, response.StatusCode, contents)
		return
	}

	responseRedirect := models.EmailRedirectInfo{}
	if err = json.Unmarshal(contents, &responseRedirect); err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, emailOnly{Email: responseRedirect.Email})
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	token, err := jwtok.ExtractToken(r)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	err = passwordCallbackTemplate.Execute(w, tokenCallback{
		Token:    token,
		Callback: "/reset_password.html",
	})

	if err != nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	}
}

func NewPasswordEnter(w http.ResponseWriter, r *http.Request) {
	tok, err := jwtok.ExtractToken(r)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	password := passwordOnly{}
	if err = json.Unmarshal(body, &password); err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	pass, _ := json.Marshal(password)

	resetPage := fmt.Sprintf("%s/password/reset?token=%s", config.ServerURL, tok)

	response, err := http.Post(resetPage, "application/json", bytes.NewBuffer(pass))
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)
	fmt.Println("Contents:", string(contents))

	if response.StatusCode != http.StatusOK {
		responses.JSON(w, response.StatusCode, contents)
		return
	}

	token := tokenOnly{}
	if err = json.Unmarshal(contents, &token); err != nil {
		responses.JSON(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, token)
}
