package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"nine/src/config"
	"nine/src/jwtok"
	"nine/src/models"
	"nine/src/responses"
)

func GetProfile(w http.ResponseWriter, r *http.Request) {
	token, err := jwtok.ExtractToken(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	getProfilePage := fmt.Sprintf("%s/users/profile?token=%s", config.ServerURL, token)

	response, err := http.Get(getProfilePage)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	defer response.Body.Close()
	contents, _ := ioutil.ReadAll(response.Body)

	profile := models.Profile{}
	err = json.Unmarshal(contents, &profile)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, response.StatusCode, profile)
}

func UpdateProfile(w http.ResponseWriter, r *http.Request) {

	client := http.Client{}
	token, err := jwtok.ExtractToken(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	updateProfilePage := fmt.Sprintf("%s/users/profile?token=%s", config.ServerURL, token)

	req, err := http.NewRequest(http.MethodPut, updateProfilePage, r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	resp, err := client.Do(req)
	contents, _ := ioutil.ReadAll(resp.Body)

	responses.JSON(w, resp.StatusCode, contents)
}
