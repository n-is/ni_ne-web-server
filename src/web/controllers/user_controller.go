package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"nine/src/config"
	"nine/src/jwtok"
	"nine/src/models"
	"nine/src/responses"
)

func GetUser(w http.ResponseWriter, r *http.Request) {
	token, err := jwtok.ExtractToken(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	getProfilePage := fmt.Sprintf("%s/users?token=%s", config.ServerURL, token)

	response, err := http.Get(getProfilePage)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	user := models.User{}
	err = json.Unmarshal(contents, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	responses.JSON(w, response.StatusCode, user)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	client := http.Client{}
	token, err := jwtok.ExtractToken(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	contents, _ := ioutil.ReadAll(r.Body)
	email := emailOnly{}
	json.Unmarshal(contents, &email)
	cnts, _ := json.Marshal(email)

	updateProfilePage := fmt.Sprintf("%s/users?token=%s", config.ServerURL, token)

	fmt.Println("Updating user info at:", config.ServerURL+"/users")
	req, err := http.NewRequest(http.MethodPut, updateProfilePage, bytes.NewReader(cnts))
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	resp, err := client.Do(req)
	contents, _ = ioutil.ReadAll(resp.Body)
	fmt.Println("Response:", string(contents), "Error: ", err)

	responses.JSON(w, resp.StatusCode, contents)
}
