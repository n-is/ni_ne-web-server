package controllers

import (
	"html/template"
	"sync"
)

var (
	tempUsersMu        sync.Mutex
	tempUsers          map[string]bool
	afterLoginCallback string

	googleCallbackTemplate   *template.Template
	passwordCallbackTemplate *template.Template
)

const (
	minOAuthStringLen  = 50
	maxOAuthStringLen  = 80
	maxRandomStringGen = 10
)

type emailOnly struct {
	Email string `json:"email"`
}

type passwordOnly struct {
	Password string `json:"password"`
}

type idOnly struct {
	ID uint32 `json:"id"`
}

type tokenOnly struct {
	Token string `json:"token"`
}

type tokenCallback struct {
	Token    string
	Callback string
}

func init() {
	tempUsersMu = sync.Mutex{}
	tempUsers = make(map[string]bool)

	googleCallbackTemplate = template.Must(template.New("").Parse(googleTokenPassTemplate))
	passwordCallbackTemplate = template.Must(template.New("").Parse(passwordTokenTemplate))
}

const googleTokenPassTemplate = `
	<html><body>
	<script>
		sessionStorage.google_auth = true
		sessionStorage.auth_token = {{.Token}}
		window.location.href = {{.Callback}}
	</script>
	</body></html>
`

const passwordTokenTemplate = `
	<html><body>
	<script>
		sessionStorage.pass_auth_token = {{.Token}}
		window.location.href = {{.Callback}}
	</script>
	</body></html>
`

func SetupAfterLoginCallback(callback string) {
	afterLoginCallback = callback
}
