package router

import (
	"github.com/gorilla/mux"
	"net/http"
	"nine/src/middlewares"
	"nine/src/web/controllers"
)

type route struct {
	Uri          string
	Method       string
	Handler      func(w http.ResponseWriter, r *http.Request)
	AuthRequired bool
}

var allRoutes = []route{
	{
		Uri:          "/login",
		Method:       http.MethodPost,
		Handler:      controllers.Login,
		AuthRequired: false,
	},
	{
		Uri:          "/users",
		Method:       http.MethodGet,
		Handler:      controllers.GetUser,
		AuthRequired: true,
	},
	{
		Uri:          "/users",
		Method:       http.MethodPut,
		Handler:      controllers.UpdateUser,
		AuthRequired: true,
	},
	{
		Uri:          "/users/profile",
		Method:       http.MethodGet,
		Handler:      controllers.GetProfile,
		AuthRequired: true,
	},
	{
		Uri:          "/users/profile",
		Method:       http.MethodPut,
		Handler:      controllers.UpdateProfile,
		AuthRequired: true,
	},
	{
		Uri:          "/password/forgot",
		Method:       http.MethodPost,
		Handler:      controllers.ForgotPassword,
		AuthRequired: false,
	},
	{
		Uri:          "/password/new",
		Method:       http.MethodPost,
		Handler:      controllers.NewPasswordEnter,
		AuthRequired: true,
	},
}

func NewRouter() *mux.Router {
	r := mux.NewRouter()

	for _, rt := range allRoutes {
		if rt.AuthRequired {
			r.HandleFunc(rt.Uri, middlewares.SetMiddlewareLogger(
				middlewares.SetMiddlewareJSON(
					middlewares.SetMiddlewareAuthentication(rt.Handler),
				),
			)).Methods(rt.Method)
		} else {
			r.HandleFunc(rt.Uri, middlewares.SetMiddlewareLogger(
				middlewares.SetMiddlewareJSON(rt.Handler),
			)).Methods(rt.Method)
		}
	}

	// Add google login and callback route
	r.HandleFunc("/google/login", controllers.GoogleLogin)
	r.HandleFunc("/google/auth", controllers.GoogleLoginCallback)

	// Password reset redirect
	r.HandleFunc("/password/reset", controllers.ResetPassword)

	// Declare the static file directory and point it to the directory we just made
	staticFileDirectory := http.Dir("src/web/assets/")
	staticFileHandler := http.StripPrefix("/", http.FileServer(staticFileDirectory))
	r.PathPrefix("/").Handler(staticFileHandler)

	return r
}
